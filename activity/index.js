
let userArr = ['Gabby','Jackal','Sherlock'];
console.log("Original Array");
console.log(userArr);
/* === Add ===*/
function addNewElement(newElement){
	userArr[userArr.length] = newElement;
}

addNewElement('Alfred');
console.log(userArr);



/*====== Get ======*/


function getItem(index){
	return userArr[index];
}
let itemFound = getItem(1);
console.log("%c" + itemFound, "color:orange");



/*======= Delete =========*/

function removeLastElement(){
	let removedElement = userArr[userArr.length - 1];
	return removedElement;
}
let element = removeLastElement();
console.log("%c" + element, "color:red") 



/*======== Update ========*/

function updateUserArr(index, update){
userArr[index] = update;
}
updateUserArr( 0,"Gabriel");
console.log(userArr);


/*=== Delete All ===*/

function deleteArr(){
	userArr=[];
}
deleteArr();
console.log(userArr);

/*=== Check if array is empty ===*/

function isArrEmpty(){
	if(userArr.length > 0){
		return false;
	}else{
		return true;
	}
}

let isUsersEmpty = isArrEmpty();
	console.log(isUsersEmpty);
