console.log("Hello Gabby!!");


let studentNumberA="2020-1923";
let studentNumberB="2020-1924";
let studentNumberC="2020-1925";
let studentNumberD="2020-1926";
let studentNumberE="2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

let studentID = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

console.log(studentID);

let grades = [96.8, 85.7, 93.2, 94.6];
console.log(grades);
let computerBrands = ["Acer","Asus","Lenovo","Neo", "RedFox", "Gateway", "Toshiba", "Fujitsu"];

let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);

let myTasks =[
	"drink HTML",
	"eat Javascript",
	"inhale CSS",
	"bake SASS"
]

console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";
let city4 = "Beijing";

let cities = [city1,city2,city3,city4];
let citySample = ["Tokyo","Manila","New York","Beijing"];
console.log(typeof cities);
console.log(typeof citySample);





/*===================*/










/* 
	Accesing Elements of a Array

	Syntax:
		arrayName[index]
	
*/

console.log(grades[0]);
console.log(computerBrands[3]);


let lakersLegend = ["Kobe", "Shag", "Lebron", "Magic","Kareem"];

console.log(lakersLegend[0]);
console.log(lakersLegend[2]);

let currentLaker = lakersLegend[2];

console.log(currentLaker);

console.log("%cArray before the reassignment","color:yellow");
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("%c Array after the reassignment","color:red");
console.log(lakersLegend);


let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length - 1 ;
console.log(bullsLegend[lastElementIndex]);


/* Adding items into an array*/
let newArr = [];
console.log(newArr);
newArr[0] = "Jenney";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);

newArr[newArr.length++] = "Lisa";
console.log("%c" + newArr , "color:blue");


// Mini activity:

/*

Part 1: Adding a value at the end of the array

	-Create a function which is able to receive a single argument and add the input at the end of the superheroes array
	-Invoke and add an argument to be passed to the function
	-Log the superheroes array in the console

	let superHeroes = ['Iron Man', 'Spiderman', 'Captain America']

*/

let superHeroes = ['Iron Man', 'Spiderman', 'Captain America'];

let createSuperHero	= function(newHero){
	superHeroes[superHeroes.length] = newHero; 
	return superHeroes;
	}


console.log("%c" + createSuperHero("Batman"),"color:yellow");
console.log("%c" + createSuperHero("Antman"),"color:yellow");
console.log(superHeroes);
	
/*
Part 2: Retrieving an element using a function
	
	-Create a function which is able to receive an index number as a single argument
	-Return the element/item accessed by the index.
	-Create a global variable named heroFound and store/pass the value returned by the function.
	-Log the heroFound variable in the console.
*/






function findHero(index){
	return superHeroes[index];
}
let heroFound = findHero(3);
console.log("%c" + heroFound, "color:orange");



for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);	
}

let numArr = [5, 12, 26, 30, 42, 50, 67, 85]

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}



// Multidimensional Arrays

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
];
console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[7][4]);
